/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.test.util;

import java.util.NoSuchElementException;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;

public class PropertiesUtilTest {

    @Test
    public void getProperty_propertyExists_returnsValue() {
        String value = PropertiesUtil.fromSpringApplication().getProperty("com.somospnt.test.sample");
        assertNotNull(value);
        assertFalse(value.isEmpty());
    }

    @Test
    public void getProperty_propertyDoesNoExist_throwsNoSuchElementException() {
        assertThatExceptionOfType(NoSuchElementException.class)
                .isThrownBy(() -> PropertiesUtil.fromSpringApplication().getProperty("com.somospnt.test.does-no-exist"));
    }

    @Test
    public void getProperty_fileDoesNoExist_throwsRuntimeExceptiontException() {
        assertThatExceptionOfType(RuntimeException.class)
                .isThrownBy(() -> PropertiesUtil.from("does-no-exist.properties").getProperty("com.somospnt.test.does-no-exist"));
    }

}
